package com.company;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Main {

    private static String fname = "./src/com/company/file.csv";
    private static final String separate = " ";
    private static String[] d = new String[12];

    public static void main(String[] args) {

        int sum_lenght = 0;
        int sum_bytes = 0;
        double sum_bit = 0;
        ReadCSV();
        System.out.printf("%-3s|%-57s|%-11s|%-39s%n", "№", "слово ", " ", "Количество информации");
        System.out.println("---------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%-4s|%-57s|%-11s|%-11s|%14s|%14s%n", " ", " ", " ", "кол-во", "байт, размер", "бит,");
        System.out.printf("%-4s|%-57s|%-11s|%-11s|%14s|%14s%n", " ", " ", "палиндром", "символов", "в программе", "по Хартли");
        for (int i = 0; i < d.length; i++) {
            sum_lenght += (d[i].length());
            sum_bytes += WordBytes(d[i]);
            sum_bit += HartleyBit(d[i]);
            System.out.println("---------------------------------------------------------------------------------------------------------------------");
            Vivod(d[i], i + 1);
        }
        System.out.println("---------------------------------------------------------------------------------------------------------------------");
        System.out.printf("%-4s|%-57s|%-11s|%-11d|%14d|%14.2f", " ", "ИТОГО ", " ", sum_lenght, sum_bytes, sum_bit);

    }

    /*чтение из файла*/
    public static void ReadCSV() {
        File file = new File(fname);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "Windows-1251"))) {
            String line = br.readLine();
            int i = 0;
            while ((line = br.readLine()) != null) {
                String[] elements = line.split(separate);
                d[i] = elements[0];
                i++;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*метод проверяет является ли слово палиндромом*/
    public static char Palindrom(String string) {

        if (string.equals((new StringBuilder(string)).reverse().toString())) {
            return '+';
        } else return '-';
    }

    /*метод считает количество байт в программе*/
    public static int WordBytes(String string) {
        final byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
        return bytes.length;
    }

    /*метод считает бит по Хартли*/
    public static double HartleyBit(String string) {
        double alphabet = string.length();
        char[] charstring = string.toCharArray();
        for (int i = 0; i < string.length(); i++) {
            for (int j = 0; j < string.length(); j++) {
                if (charstring[i] == charstring[j]) {
                    if (i != j) {
                        if (j > i) {
                            continue;
                        }
                        alphabet -= 1;
                        charstring[j] = 0;
                    }
                }
            }
        }
        return string.length() * (Math.log(alphabet) / Math.log(2));
    }


    /*метод выводит значения по каждому слову*/
    public static void Vivod(String word, int i) {
        System.out.printf("%-4d|%-57s|%-11s|%-11d|%14d|%14.2f%n", i, word, Palindrom(word), word.length(), WordBytes(word), HartleyBit(word));
    }


}








