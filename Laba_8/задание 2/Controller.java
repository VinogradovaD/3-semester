package com.company;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;

import static javafx.scene.paint.Color.*;

public class Controller {
    @FXML
    private TextField line_x1, line_x2, line_y1, line_y2;
    @FXML
    private TextField rect_x, rect_y, rect_h, rect_w;
    @FXML
    private TextField ell_x, ell_y, ell_h, ell_w;
    @FXML
    private TextField tr_x1, tr_x2, tr_x3, tr_y1, tr_y2, tr_y3;
    @FXML
    private Button button_line, button_rect, button_ell, button_tr;
    @FXML
    private GraphicsContext context;
    @FXML
    private Canvas canvas_line, canvas_rect, canvas_ell, canvas_tr;

    @FXML
    void line() {
        try {
            context = canvas_line.getGraphicsContext2D ();
            double x1 = Double.parseDouble (line_x1.getText ());
            double x2 = Double.parseDouble (line_x2.getText ());
            double y1 = Double.parseDouble (line_y1.getText ());
            double y2 = Double.parseDouble (line_y2.getText ());
            context.setStroke (BLACK);
            context.strokeLine (x1, x2, y1, y2);
        } catch (NumberFormatException e) {
            Alert alert = new Alert (Alert.AlertType.INFORMATION);
            alert.setTitle ("Ошибка!");
            alert.setHeaderText (null);
            alert.setContentText ("Неверный формат числа.");
            alert.showAndWait ();
        }
    }

    @FXML
    void rectangle() {
        try {
            context = canvas_rect.getGraphicsContext2D ();
            double x = Double.parseDouble (rect_x.getText ());
            double y = Double.parseDouble (rect_y.getText ());
            double h = Double.parseDouble (rect_h.getText ());
            double w = Double.parseDouble (rect_w.getText ());
            context.setFill (BLACK);
            context.fillRect (x, y, w, h);

        } catch (NumberFormatException e) {
            Alert alert = new Alert (Alert.AlertType.INFORMATION);
            alert.setTitle ("Ошибка!");
            alert.setHeaderText (null);
            alert.setContentText ("Неверный формат числа.");
            alert.showAndWait ();
        }
    }

    @FXML
    void ellipse() {
        try {
            context = canvas_ell.getGraphicsContext2D ();
            double x = Double.parseDouble (ell_x.getText ());
            double y = Double.parseDouble (ell_y.getText ());
            double h = Double.parseDouble (ell_h.getText ());
            double w = Double.parseDouble (ell_w.getText ());
            context.setFill (BLACK);
            context.fillOval (x, y, w, h);

        } catch (NumberFormatException e) {
            Alert alert = new Alert (Alert.AlertType.INFORMATION);
            alert.setTitle ("Ошибка!");
            alert.setHeaderText (null);
            alert.setContentText ("Неверный формат числа.");
            alert.showAndWait ();
        }
    }

    @FXML
    void triangle() {
        try {
            context = canvas_tr.getGraphicsContext2D ();
            double x1 = Double.parseDouble (tr_x1.getText ());
            double x2 = Double.parseDouble (tr_x2.getText ());
            double x3 = Double.parseDouble (tr_x3.getText ());
            double y1 = Double.parseDouble (tr_y1.getText ());
            double y2 = Double.parseDouble (tr_y2.getText ());
            double y3 = Double.parseDouble (tr_y3.getText ());
            double[] x = new double[]{x1, x2, x3};
            double[] y = new double[]{y1, y2, y3};
            context.setFill (BLACK);
            context.fillPolygon (x, y, 3);

        } catch (NumberFormatException e) {
            Alert alert = new Alert (Alert.AlertType.INFORMATION);
            alert.setTitle ("Ошибка!");
            alert.setHeaderText (null);
            alert.setContentText ("Неверный формат числа.");
            alert.showAndWait ();
        }
    }

}


