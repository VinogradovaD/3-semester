package com.company;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;

import java.util.Random;

import static java.lang.Math.*;
import static javafx.scene.paint.Color.color;

public class Controller {
    @FXML
    private Canvas canvas;
    @FXML
    private GraphicsContext context;
    @FXML
    private Button button;

    @FXML
    private void click() {
        Random random = new Random ();
        float r = random.nextFloat ();
        float g = random.nextFloat ();
        float b = random.nextFloat ();
        int figures = (int) (Math.random () * 7);
        context = canvas.getGraphicsContext2D ();
        if (figures == 0) {
            context.setFill (color (r, g, b, 1));
            context.fillOval (random () * 100, random () * 100, random () * 100, random () * 100);
        } else if (figures == 1) {
            context.setStroke (color (r, g, b, 1));
            context.strokeOval (Math.random () * 100, Math.random () * 100, Math.random () * 200, Math.random () * 200);
        } else if (figures == 2) {
            context.setFill (color (r, g, b, 1));
            context.fillRect (Math.random () * 100, Math.random () * 100, Math.random () * 200, Math.random () * 200);
        } else if (figures == 3) {
            context.setStroke (color (r, g, b, 1));
            context.strokeRect (Math.random () * 100, Math.random () * 100, Math.random () * 200, Math.random () * 200);
        } else if (figures == 4) {
            context.setFill (color (r, g, b, 1));
            context.fillPolygon (
                    new double[]{Math.random () * 100, Math.random () * 100, Math.random () * 100}, // X координаты вершин
                    new double[]{Math.random () * 100, Math.random () * 100, Math.random () * 100}, // Y координаты вершин
                    3
            );
        } else if (figures == 5) {
            context.setStroke (color (r, g, b, 1));
            context.strokePolygon (
                    new double[]{Math.random () * 100, Math.random () * 100, Math.random () * 100}, // X координаты вершин
                    new double[]{Math.random () * 100, Math.random () * 100, Math.random () * 100}, // Y координаты вершин
                    3
            );
        } else if (figures == 6) {
            context.setStroke (color (r, g, b, 1));
            context.strokeLine (Math.random () * 200, Math.random () * 100, Math.random () * 100, Math.random () * 100);
        }
    }
}
