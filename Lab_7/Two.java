package com.company;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;


public class Two extends Application {

    public static void main(String[] args) {

        launch (args);
    }

    public void start(Stage stage) {

        Label label = new Label ("Координаты точки на плоскости");
        Text text_x = new Text (" X=  ");
        Text text_y = new Text (" Y=  ");
        TextField x = new TextField ();
        TextField y = new TextField ();
        Button button = new Button ("Проверить");
        ImageView image = new ImageView (new Image (Two.class.getResourceAsStream ("image.jpg")));
        image.setFitHeight (200);
        image.setFitWidth (200);
        Label point = new Label ();


        HBox hBox_x = new HBox (text_x, x);
        HBox hBox_y = new HBox (text_y, y);
        VBox vBox = new VBox (label, hBox_x, hBox_y, button);
        HBox hBoxxy = new HBox (image, vBox);
        hBoxxy.setPadding (new Insets (10));
        VBox vBox1 = new VBox (hBoxxy, point);

        Scene scene = new Scene (vBox1);
        stage.setScene (scene);
        stage.setTitle ("Попадение в область");
        stage.setWidth (450);
        stage.setHeight (300);
        stage.show ();

        button.setOnAction (new EventHandler<ActionEvent> () {
            @Override
            public void handle(ActionEvent actionEvent) {
                Function (x, y);
                if (Function (x, y) == true) {
                    point.setText ("Точка (" + x.getText () + "," + y.getText () + ") принадлежит заданной области");
                    point.setTextFill (Color.BLUE);
                }
                if (Function (x, y) == false) {
                    point.setText ("Точка (" + x.getText () + "," + y.getText () + ") не принадлежит заданной области");
                    point.setTextFill (Color.RED);
                }
            }
        });
    }

    public static boolean Function(TextField x, TextField y) {
        try {
            double x1 = Double.parseDouble (x.getText ());
            double y1 = Double.parseDouble (y.getText ());
            double R = Math.sqrt (x1 * x1 + y1 * y1);
            double S = 2 * x1 + 6;
            if (x1 >= 0 && (y1 > 0 || y1 <= 0)) {
                if ((R <= 6) && (x1 <= 3)) {
                    return true;
                } else return false;
            }
            if (x1 < 0 && y1 > 0) {
                if ((y1 <= S) && x1 >= -3 && y1 <= 6) {
                    return true;
                } else
                    return false;
            }
        } catch (NumberFormatException e) {
            System.out.println ("Ошибка");
        }
        return false;
    }
}




