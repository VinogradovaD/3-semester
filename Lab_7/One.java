package com.company;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.util.ArrayList;

public class One extends Application {

    static double d;
    static ArrayList<String> arrayList = new ArrayList<> ();

    public static void main(String[] args) {

        launch (args);
    }

    public void start(Stage stage) {

        Label label = new Label ("Введите коэффициенты");
        label.setAlignment (Pos.BASELINE_CENTER);
        label.setMaxWidth (Double.MAX_VALUE);
        TextField a = new TextField ();
        TextField b = new TextField ();
        TextField c = new TextField ();
        Button button = new Button ("Вычислить");
        Text a1 = new Text ("x² + ");
        Text b1 = new Text ("x + ");
        Text c1 = new Text ("=0 ");
        Text d1 = new Text ();
        Text x = new Text ();

        HBox hbox1 = new HBox (a, a1, b, b1, c, c1);
        VBox vbox = new VBox (label, hbox1, d1, x, button);

        Scene scene = new Scene (vbox);
        stage.setScene (scene);
        stage.setTitle ("Решение квадратного уравнения");
        stage.setWidth (400);
        stage.setHeight (250);
        stage.show ();

        button.setOnAction (new EventHandler<ActionEvent> () {
            @Override
            public void handle(ActionEvent actionEvent) {
                equation (a, b, c);
                if (!arrayList.isEmpty ()) {
                    x.setText ("Корни:" + arrayList.toString ());
                    d1.setText ("D:" + d);
                    arrayList.clear ();

                }
            }
        });
    }

    public static ArrayList equation(TextField a, TextField b, TextField c) {
        try {
            double a1 = Double.parseDouble (a.getText ());
            double b1 = Double.parseDouble (b.getText ());
            double c1 = Double.parseDouble (c.getText ());
            d = b1 * b1 - 4 * a1 * c1;
            if (d == 0) {
                arrayList.add (String.format ("%.2f", ((-b1 + Math.sqrt (d)) / (2 * a1))));
            }
            if (d > 0) {
                arrayList.add (String.format ("%.2f", ((-b1 + Math.sqrt (d)) / (2 * a1))));
                arrayList.add (String.format ("%.2f", ((-b1 + Math.sqrt (d)) / (2 * a1))));
            }
            if (d < 0) {
                String first = String.format ("%.2f", -b1 / (2 * a1));
                String second = String.format ("%.2f", Math.sqrt (-d) / (2 * a1));
                arrayList.add (first + "±i" + second);

            }
        } catch (NumberFormatException e) {
            System.out.println ("Ошибка");
        }

        return arrayList;
    }


}

