package com.company;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Three extends Application {
    ObservableList<String> words1 = FXCollections.observableArrayList ("Кузнечик", "Вася", "Крокодил", "Орел", "Трактор", "Боинг 747", "Слоник", "Ильвар");
    ObservableList<String> words2 = FXCollections.observableArrayList ("ушел", "полетел", "побежал", "залез", "уполз", "упрыгал", "умчался");
    ObservableList<String> words3 = FXCollections.observableArrayList ("в кусты", "на дискотеку", "домой", "за пивом", "на Чукотку", "по грибы", "на охоту");

    public static void main(String[] args) {

        Application.launch (args);
    }

    public void start(Stage stage) {
        ComboBox<String> comboBox1 = new ComboBox<String> (words1);
        comboBox1.setValue (words1.get (0));
        Label label1 = new Label ();
        label1.setText (words1.get (0));
        comboBox1.setOnAction (event -> label1.setText (comboBox1.getValue ()));

        ComboBox<String> comboBox2 = new ComboBox<String> (words2);
        comboBox2.setValue (words2.get (0));
        Label label2 = new Label ();
        label2.setText (words2.get (0));
        comboBox2.setOnAction (event -> label2.setText (comboBox2.getValue ()));

        ComboBox<String> comboBox3 = new ComboBox<String> (words3);
        comboBox3.setValue (words3.get (0));
        Label label3 = new Label ();
        label3.setText (words3.get (0));
        comboBox3.setOnAction (event -> label3.setText (comboBox3.getValue ()));

        TextArea textArea = new TextArea ();

        Button addPhrase = new Button ("Добавить фразу");
        Button close = new Button ("Закрыть");


        HBox hBox = new HBox (comboBox1, comboBox2, comboBox3);
        VBox vBox = new VBox (hBox, textArea, addPhrase, close);


        Scene scene = new Scene (vBox);
        stage.setScene (scene);
        stage.setTitle ("Электронный писатель");
        stage.setWidth (400);
        stage.setHeight (250);
        stage.show ();

        addPhrase.setOnAction (new EventHandler<ActionEvent> () {
            @Override
            public void handle(ActionEvent actionEvent) {
                textArea.setText (textArea.getText () + label1.getText () + " " + label2.getText () + " " + label3.getText () + "\n");
            }
        });
        close.setOnAction (new EventHandler<ActionEvent> () {
            @Override
            public void handle(ActionEvent actionEvent) {
                Platform.exit ();
            }
        });
    }
}


