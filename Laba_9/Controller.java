package com.company;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.fxml.FXML;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.*;
import java.util.Collections;

public class Controller {
    private static ObservableList<Date> tempDataYear = FXCollections.observableArrayList ();

    @FXML
    private TableView<Date> tableDateYear, tableDateMonth;
    @FXML
    private TableColumn<Date, String> dateColumnYear, dateColumnMonth;
    @FXML
    private TableColumn<Date, String> tempColumnYear, tempColumnMonth;
    @FXML
    ComboBox comboBox;
    @FXML
    private BarChart<String, Integer> barChart;
    @FXML
    private LineChart<String, Integer> lineChart;

    @FXML
    void initialize() {
        ReadCSV ();
        TableOfYear ();
        BarChart ();
    }

    @FXML
    void ComboboxAction() {
        LineChart ();
        TableOfMonth ();
    }

    @FXML
    void TableOfYear() {
        dateColumnYear.setCellValueFactory (new PropertyValueFactory<Date, String> ("date"));
        tempColumnYear.setCellValueFactory (new PropertyValueFactory<Date, String> ("temp"));
        tableDateYear.setItems (tempDataYear);
    }

    @FXML
    void TableOfMonth() {
        ObservableList<Date> tempDataMonth = FXCollections.observableArrayList ();
        String boxValue = NumberOfMonth ((String) comboBox.getValue ());
        for (int i = 0; i < tempDataYear.size (); i++) {
            if (tempDataYear.get (i).getMonth ().equals (boxValue))
                tempDataMonth.add (new Date (tempDataYear.get (i).getDay (), tempDataYear.get (i).getTemp ()));
        }
        dateColumnMonth.setCellValueFactory (new PropertyValueFactory<Date, String> ("day"));
        tempColumnMonth.setCellValueFactory (new PropertyValueFactory<Date, String> ("temp"));
        tableDateMonth.setItems (tempDataMonth);
    }

    void ReadCSV() {
        try (BufferedReader br = new BufferedReader (new FileReader ("./src/com/company/temp2.csv"))) {
            String line;
            while ((line = br.readLine ()) != null) {
                tempDataYear.add (new Date (line.substring (0, 2), line.substring (3, 5), line.substring (6, 10), line.substring (11)));
            }
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

    @FXML
    void BarChart() {
        XYChart.Series<String, Integer> dataSeries = new XYChart.Series<String, Integer> ();
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Январь", Average (1)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Февраль", Average (2)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Март", Average (3)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Апрель", Average (4)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Май", Average (5)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Июнь", Average (6)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Июль", Average (7)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Август", Average (8)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Сентябрь", Average (9)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Октябрь", Average (10)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Ноябрь", Average (11)));
        dataSeries.getData ().add (new XYChart.Data<String, Integer> ("Декабрь", Average (12)));
        barChart.getData ().add (dataSeries);
    }

    int Average(Integer month) {
        int sum = 0;
        int k = 0;
        for (int i = 0; i < tempDataYear.size (); i++) {
            if (Integer.parseInt (tempDataYear.get (i).getMonth ()) == month) {
                sum += Integer.parseInt (tempDataYear.get (i).getTemp ());
                k++;
            }
        }
        sum = sum / k;
        return sum;
    }

    @FXML
    void LineChart() {
        lineChart.getData ().removeAll (Collections.singleton (lineChart.getData ().setAll ()));
        String month = (String) comboBox.getValue ();
        String boxValue = NumberOfMonth (month);
        XYChart.Series series = new XYChart.Series ();
        series.setName (month);
        for (int i = 0; i < tempDataYear.size (); i++) {
            if (tempDataYear.get (i).getMonth ().equals (boxValue))
                series.getData ().add (new XYChart.Data (tempDataYear.get (i).getDay (), Integer.parseInt (tempDataYear.get (i).getTemp ())));
        }
        lineChart.getData ().add (series);

    }

    String NumberOfMonth(String month) {
        String num = "02";
        if (month.equals ("Январь")) {
            num = "01";
        }
        if (month.equals ("Февраль")) {
            num = "02";
        }
        if (month.equals ("Март")) {
            num = "03";
        }
        if (month.equals ("Апрель")) {
            num = "04";
        }
        if (month.equals ("Май")) {
            num = "05";
        }
        if (month.equals ("Июнь")) {
            num = "06";
        }
        if (month.equals ("Июль")) {
            num = "07";
        }
        if (month.equals ("Август")) {
            num = "08";
        }
        if (month.equals ("Сентябрь")) {
            num = "09";
        }
        if (month.equals ("Октябрь")) {
            num = "10";
        }
        if (month.equals ("Ноябрь")) {
            num = "11";
        }
        if (month.equals ("Декабрь")) {
            num = "12";
        }
        return num;
    }

}