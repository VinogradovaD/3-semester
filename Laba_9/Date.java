package com.company;

public class Date {
    private String day;
    private String month;
    private String year;
    private String temp;
    private String date;

    public Date(String day, String month, String year, String temp) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.temp = temp;
        this.date = day + "." + month + "." + year;
    }

    public Date(String day, String temp) {
        this.day = day;
        this.temp = temp;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getDate() {
        return date;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}


